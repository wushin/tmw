#!/bin/bash

if [ ! -f ./server/sqlite3/mana.db ]
then
  cp ./server/sqlite3/mana.db.base server/sqlite3/mana.db
fi

if [ ! -f ./server/game-data/manaserv.xml ]
then
  cp ./server/game-data/manaserv.xml.example ./server/game-data/manaserv.xml
fi

sudo docker-compose up -d --build --remove-orphans
