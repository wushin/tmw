#!/bin/bash

read -p "Who do you want to give admin status? " -r
echo ""
USERNAME=$REPLY
read -p "Are you sure you want to give $USERNAME admin? [y/N] " -n 1 -r
echo ""
if [[ $REPLY =~ ^[Yy]$ ]]
then
  sudo docker exec account bash -c "sqlite3 /server/sqlite3/mana.db \"UPDATE mana_accounts SET level=255 WHERE username='$USERNAME';\""
fi

